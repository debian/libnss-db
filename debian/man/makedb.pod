=head1 NAME

makedb - Create simple DB database from textual input.

=head1 SYNOPSIS

B<makedb> B<[> I<options> B<]> I<INPUT-FILE> I<OUTPUT-FILE>

B<makedb> B<[> I<options> B<]> B<-o> I<OUTPUT-FILE> I<INPUT-FILE>

B<makedb> B<[> I<options> B<]> B<-u> I<INPUT-FILE>

=head1 DESCRIPTION

This program is mainly used to generate database files for the
libnss_db.so module for the Name Service Switch.

=head1 OPTIONS

=over 8

=item B<-f>, B<--fold-case>

Convert key to lower case.

=item B<-o>, B<--output> I<OUTPUT-FILE>

Write output to file I<OUTPUT-FILE>

=item B<-q>, B<--quiet>

Do not print messages while building database.

=item B<-u>, B<--undo>

Print content of database file, one entry a line

=item B<-?>, B<--help>

Give help message.

=item B<--usage>

Give a short usage message.

=item B<-V>, B<--version>

Print program version.

=back

=head1 AUTHOR

I<makedb> was written by Ulrich Drepper for the GNU C Library.

This manpage was written by Ben Collins E<lt>bcollins@debian.orgE<gt> for
the Debian GNU/Linux system.
